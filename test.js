let selectedRow = null;

function onFormSubmit(e) {
    event.preventDefault();
    const student = readData(Student);
    if (selectedRow == null) {
        insertRecord(student);
    }
    else {
        updateRecord(student);
    }
    resetForm();
}

// lire des données 
function readData(Student) {
    const newStudent = new Student();
    newStudent.lastname = document.getElementById('lastname').value;
    newStudent.firstname = document.getElementById('firstname').value;
    newStudent.birthdate = document.getElementById('birthdate').value;
    newStudent.age = ageCount(newStudent.birthdate);
    newStudent.email = document.getElementById('email').value;
    newStudent.numberPhone = document.getElementById('numberPhone').value;
    console.log(newStudent);
    return newStudent;
}


// fonction pour insérer les données
function insertRecord(student) {
    const table = document.getElementById('myTable').getElementsByTagName('tbody')[0];
    const newRow = document.createElement('tr');
    for (let key in student) {
        let td = document.createElement('td');
        if (key === 'age') {
            td.innerHTML = student[key];
        } else {
            td.innerHTML = student[key];
        }
        newRow.appendChild(td);
    }
    const td6 = document.createElement('td');
    td6.innerHTML = `<button id = 'edit' onclick='onEdit(this)'>Edit</button> <button id = 'delete'  onclick='onDelete(this)'>Delete</button> `;
    newRow.appendChild(td6);
    table.appendChild(newRow);
}



function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById('lastname').value = selectedRow.cells[0].innerHTML;
    document.getElementById('firstname').value = selectedRow.cells[1].innerHTML;
    document.getElementById('birthdate').value = selectedRow.cells[2].innerHTML;
    document.getElementById('email').value = selectedRow.cells[4].innerHTML;
    document.getElementById('numberPhone').value = selectedRow.cells[5].innerHTML;
    console.log('fait ! ')
}


// fonction pour mettre a jour
function updateRecord(student) {
    let selectedRow = document.querySelector('#myTable tbody .selected')
    let cells = selectedRow.getElementsByTagName("td");
    let i = 0;
    for (let data in student) {
        if (student[dat] && student[data].trim() !== "") {
            if (data === 'age') {
               cells[i].value.innerHTML = student[data]
            } else {
                cells[i].value.innerHTML = student[data];
            }
            i++;

        }
    }
}



// fonction pour supprimer les données 
function onDelete(td) {
    if (confirm('voulez-vous vraiment supprimer cet étudiants ?')) {
        let row = td.parentElement.parentElement;
        document.getElementById('myTable').deleteRow(row.rowIndex);
        resetForm();
    }
}

// fonction pour réinitialiser les données
function resetForm() {
    let inputFields = document.querySelectorAll('#formId input');
    for (let i = 0; i < inputFields.length; i++) {
        inputFields[i].value = '';
    }
    if (selectedRow) {
        selectedRow.classList.remove('selected');
        selectedRow = null;
    }
}

// class Student
class Student {
    constructor(lastname, firstname, birthdate, email, numberPhone) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.age = function () { return ageCount(this.birthdate) };
        this.email = email;
        this.numberPhone = numberPhone;
    }
}


// fonction calcul age 
function ageCount(birthdate) {
    let now = new Date();
    let birth = new Date(birthdate);
    let age = now.getFullYear() - birth.getFullYear();
    let month = now.getMonth() - birth.getMonth();
    if (month < 0 || (month === 0 && now.getDate() < birth.getDate())) {
        age--;
    }
    return age;
}
